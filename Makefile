# Compile and link flags
CXX          =  g++
CFLAGS       = -Wall
LDFLAGS = -lstdc++
# Compilation (add flags as needed)
CXXFLAGS    += `/usr/local/bin/pkg-config opencv --cflags`

# Linking (add flags as needed)
LDFLAGS     += `/usr/local/bin/pkg-config opencv --libs`
LDFLAGS += -lgdal

# Name your target executables here
all         = buscacos

	# Default target is the first one - so we will have it make everything :-)
all: $(all)

clean:
	rm -f $(all) *.o

# Program dependencies (.o files will be compiles by implicit rules)
buscacos : buscacos.o
