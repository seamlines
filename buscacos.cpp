#ifdef _MSC_VER
#include "stdafx.h"
#endif

//#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <exception>
//#include "cxcore.h"
#include "opencv/cv.h"
#include "opencv/highgui.h"

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>

#include "gdal_priv.h"
#include "cpl_conv.h" // for CPLMalloc()

using namespace cv;

////////////////
// EXCEPTIONS //
////////////////
class CosException : public std::exception
{
  const char* m_what;
  virtual const char* what() { return m_what; }
public:
  CosException(const char* what) : m_what(what) {}
};

///////////
// GRAPH //
///////////
struct Vertex
{
  int x, y;
  unsigned int color, count;

  Vertex() : x(0), y(0), count(0) {}
};

struct Edge
{
  float weight;
  unsigned x1, y1, x2, y2;

  void drawLine(Mat& img, int thickness=1)
  {
    line(img, Point2i(x1, y1), Point2i(x2, y2), Scalar::all(255), thickness);
  }
};

typedef boost::adjacency_list<
  boost::listS,
  boost::vecS,
  boost::undirectedS,
  Vertex,
  Edge> Graph;

typedef Graph::vertex_descriptor VertexID;
typedef Graph::edge_descriptor EdgeID;

////////////
// OPENCV //
////////////

typedef Rect_<int> Rect2i;
typedef Rect_<double> Rect2d;

void read_cost_img(char* img_name, Mat& result)
{
  Mat img = imread("PP.tif");
  if(!img.data)
    {
      throw CosException("Error obrint la imatge");
    }

  Mat ch0(img.rows, img.cols, CV_8UC1);
  int from_to[] = { 0,0 };
  mixChannels(&img, 1, &ch0, 1, from_to, 1);

  absdiff(ch0, Scalar(127), result);
}

int minimax_cost(Mat& img, Point2i startPoint, Point2i endPoint, Mat& imgFlooded)
{
  uchar endVal;
  bool bConnected = false;
  
  img.at<uchar>(startPoint.x, startPoint.y) = 1;
  int iMax = 127, iMin = 0, iMid = -1; // 127 per la manera com ta skalat
  int iLastConnected = -1;
  while(iMax >= iMin)
    {
      iMid = (iMax + iMin) / 2;
      
      printf("Provant valor: %d\n", iMid);
      Scalar hiDiff = Scalar(iMid);
      
      img.copyTo(imgFlooded);

      floodFill(imgFlooded, startPoint, Scalar(255), 0,
                hiDiff, hiDiff, 4 | FLOODFILL_FIXED_RANGE);
      
      endVal = imgFlooded.at<uchar>(endPoint.y, endPoint.x);
      printf("End(%d, %d) DL=(%d)\n", endPoint.x, endPoint.y, endVal );
      bConnected = (endVal == 255);
      if(bConnected)
        {
          iLastConnected = iMid;
          iMax = iMid - 1;
        }
      else
        {
          iMin = iMid + 1;
        }         
    }

  return iLastConnected;
  
}



inline unsigned int pseudo_random()
{
	static unsigned int v = rand();
	static unsigned int u = rand();
	v = 36969*(v & 65535) + (v >> 16);
	u = 18000*(u & 65535) + (u >> 16);
	return (v << 16) + u;
}



void get_watershed_markers(Mat& costImg, int iMaxCost, Mat& markers, Mat& imgFlooded)
{
	Mat imgMaxLevel;
	Mat threshold(costImg.size(), CV_16U);
	threshold=(costImg == iMaxCost);

	Mat_<uchar>::iterator it, itEnd = threshold.end<uchar>();
	unsigned int count = 0;
	unsigned int max_count = INT_MAX;
	while(max_count > 32768)
	{
		max_count = 0;
		for(it=threshold.begin<uchar>(), count = 0; it != itEnd; it++, count++)
		{
			if((pseudo_random() % 2) == 0) *it=0;
			if(*it != 0)
				*it += ++max_count;
		}
	}

	printf("pixels set %d\n", max_count);

	/*
	for(int y = 0; y < imgMaxLevel.rows; y++)
    {
		imgFlooded.row(y) = Scalar(0);
		if(y%3 == 2) y += 3;
    }
	*/

    imwrite("threshold.tif", threshold);
	
    Mat element = getStructuringElement(MORPH_ELLIPSE, Size(5, 5), Point(2, 2));
	dilate(threshold, imgMaxLevel, element, Point(-1, -1), 2);
    imwrite("maxlevel.tif", imgMaxLevel);
	imgMaxLevel.convertTo(markers, CV_32S);

/*Mat floodedDilated;
  erode(imgFlooded != 255, floodedDilated, element, Point(-1, -1), 3);
  Mat tmp = //((costImg >= iMaxCost) - (costImg > iMaxCost + 50) + (floodedDilated == 255));
    ((costImg == iMaxCost)  + (floodedDilated == 255));
  Mat eroded, closed;
  erode(tmp, imgMaxLevel, element, Point(-1, -1), 1); // (costImg >= 22) - (costImg > 32)
  dilate(eroded, imgMaxLevel, element, Point(-1, -1), 1);
  //imgMaxLevel = (costImg == iMaxCost) + (floodedDilated == 255);*/
/*
  vector<vector<Point> > contours;
  vector<Vec4i> hierarchy;
  findContours(imgMaxLevel, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

  markers = Mat(imgMaxLevel.size(), CV_32S);
  markers = Scalar::all(0);
  int idx = 0;
  for(int compCount=1 ; idx >= 0; idx = hierarchy[idx][0], compCount++ )
    drawContours(markers, contours, idx, Scalar::all(compCount + 1), -1, 8, hierarchy, INT_MAX);
  */
}

void get_watershed_relief(Mat& costImg, Mat& imgFlooded, Mat& reliefBGR)
{
  Mat th, tmp;
  threshold(imgFlooded, th, 254, 255, THRESH_BINARY_INV);
  bitwise_or(costImg, th, tmp);
  Mat relief;

  //XXXX: OJUUU!!!
  subtract(Scalar(255), costImg, relief);
  
  cvtColor(relief, reliefBGR, CV_GRAY2BGR); // 
}

void get_raster_graph(Mat& costImg, Mat& raster_graph, Point2i startPoint, Point2i endPoint)
{
  Mat imgFlooded, markers, reliefBGR;

  printf("Rows=%d, Cols=%d\n", costImg.rows, costImg.cols);
  int iMaxCost = minimax_cost(costImg, startPoint, endPoint, imgFlooded);
  printf("Cost trobat: %d\n", iMaxCost);

  get_watershed_markers(costImg, iMaxCost, markers, imgFlooded);

  get_watershed_relief(costImg, imgFlooded, reliefBGR);
  imwrite("RELIEF.tif", reliefBGR);
  watershed(reliefBGR, markers);
  
  Mat tmp = ((markers <= 0)); //  - (imgFlooded != 255)
  tmp.convertTo(raster_graph, CV_8U, 1, 0);

  imwrite("MARKERS.tif", markers); 
  imwrite("RASTER_GRAPH_ORIG.tif", raster_graph); 
  
}

void mark_vertexs(Mat& raster_graph, Mat& vertexs)
{
  vertexs = Mat::zeros(raster_graph.rows,
                           raster_graph.cols,
                           CV_16U);

  unsigned short label = 1;
  for(int y = 1; y < raster_graph.rows -1; y++)
    {
      uchar* prevRow = raster_graph.ptr<uchar>(y-1);
      uchar* thisRow = raster_graph.ptr<uchar>(y);      
      uchar* nextRow = raster_graph.ptr<uchar>(y+1);
      for(int x = 1; x < raster_graph.cols - 1; x++)
        {
          if((thisRow[x] == 255) &&
             ((prevRow[x-1] + prevRow[x] + prevRow[x+1] +
               thisRow[x-1] + thisRow[x] + thisRow[x+1] +
               nextRow[x-1] + nextRow[x] + nextRow[x+1]) >= 255*4))
            {
              vertexs.at<unsigned short>(y,x) = label++;
            }
        }
    }
}


void label_dilated_vertexs(Mat& vertexs, Mat& vertex_labels, Graph& graph)
{
  Mat dilated_vertexs, tmp;
  Mat element = getStructuringElement(MORPH_RECT, Size(3, 3), Point(2, 1));  
  dilate(vertexs, dilated_vertexs, element);
  
  vector<vector<Point> > vertex_contours;
  vector<Vec4i> hierarchy;
  findContours(dilated_vertexs, vertex_contours, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
  printf("%d arestes trobades\n", vertex_contours.size());
  
  vertex_labels = Mat(dilated_vertexs.size(), CV_32S);
  vertex_labels = Scalar::all(0);
  int idx = 0;
  
  // Per a que el color coincideixi amb l'index... (facilita cerca)
  boost::add_vertex(graph);
  boost::add_vertex(graph);
  for(int color=2 ; idx >= 0; idx = hierarchy[idx][0], color++ )
    {
      VertexID vID= boost::add_vertex(graph);
      graph[vID].color = color;
      drawContours(vertex_labels, vertex_contours, idx, Scalar::all(color), -1, 8, hierarchy, INT_MAX);
    }
  
}

void follow_edge(Mat& disconnected, int startx, int starty, Mat& cost_img, Mat& vertex_labels, Graph& graph)
{
  int x=startx, y=starty, sum=0, count=1;

  bool bFound = true;
  while(bFound) {
    bFound = false;
    for(int dy = -1; !bFound && (dy <= 1); dy++)
      for(int dx = -1; !bFound && dx <= 1; dx++)
        if(disconnected.at<uchar>(y+dy, x+dx) == 255)
          {
            disconnected.at<uchar>(y, x) = 128;
            sum += cost_img.at<uchar>(y,x);
            count++;
            x += dx;
            y += dy;
            //            printf("%d, %d\n", x, y);
            bFound = true;
          }
  }
  
  sum += disconnected.at<uchar>(y, x);
  disconnected.at<uchar>(y, x) = 128;
  
  if(count > 1)
    {
      VertexID v1 = vertex_labels.at<unsigned short>(starty, startx);
      VertexID v2 = vertex_labels.at<unsigned short>(y, x);
      if(v1 > 1 && v2 > 1 && (v1 != v2))
        {
          float mean = float(sum) / count;
          //printf("Edge> %d, %d Mitjana=%f\n", v1, v2, mean);
      
          bool ok;
          EdgeID edge;
          boost::tie(edge, ok) = boost::add_edge(v1, v2, graph);
          graph[edge].x1 = startx; graph[edge].y1 = starty;
          graph[edge].x2 = x; graph[edge].y2 = y;
          graph[edge].weight = mean;
        }
    }
}

void build_edges(Mat& disconnected, Mat& costImg, Mat& vertex_labels, Graph& graph)
{
  for(int y = 1; y < disconnected.rows -1; y++)
    {
      uchar* prevRow = disconnected.ptr<uchar>(y-1);
      uchar* thisRow = disconnected.ptr<uchar>(y);      
      uchar* nextRow = disconnected.ptr<uchar>(y+1);
      for(int x = 1; x < disconnected.cols - 1; x++)
        {
          /* Aqui no se pq el valor de fons es un 1 i no un 0... */
          if((thisRow[x] > 1) &&
             ((prevRow[x-1] > 1) + (prevRow[x] > 1) + (prevRow[x+1] > 1)+
              (thisRow[x-1] > 1) + (thisRow[x] > 1) + (thisRow[x+1] > 1)+
              (nextRow[x-1] > 1) + (nextRow[x] > 1) + (nextRow[x+1] > 1)) == 2)
            {
				follow_edge(disconnected, x, y, costImg, vertex_labels, graph);
            }
          
          // Pel que hem vist, encara quedaria fer un analisi dels
          // pixels aillats en el grafic per intentar connectar-los a
          // l'edge mes proper...
        }
    }
}

void calculate_dijkstra(vector<int>& p,
                        VertexID& from_vertex, VertexID& to_vertex,
                        Point2i startPoint, Point2i endPoint,
                        Mat& disconnected,
                        Graph& graph)
{
  Graph::edge_iterator edgeIt, edgeEnd;
  boost::tie(edgeIt, edgeEnd) = edges(graph);
  for(; edgeIt != edgeEnd; ++edgeIt)
    {
      Edge& edge = graph[*edgeIt];
      edge.drawLine(disconnected);
      
      Vertex& v1 = graph[source(*edgeIt, graph)];
      v1.x += edge.x1; v1.y += edge.y1;
      v1.count++;
      
      Vertex& v2 = graph[target(*edgeIt, graph)];
      v2.x += edge.x2; v2.y += edge.y2;
      v2.count++;
    }

  // Busquem els vertexs mes propers per fer el Dijkstra
  Graph::vertex_iterator vertexIt, vertexEnd;
  boost::tie(vertexIt, vertexEnd) = vertices(graph);
  unsigned int start_dist = UINT_MAX, end_dist = UINT_MAX;
  for(; vertexIt != vertexEnd; ++vertexIt)
    {
      Vertex& v = graph[*vertexIt];
      if(v.count < 1)
        continue;
      v.x /= v.count;
      v.y /= v.count;
      unsigned int curr_dist_start =
        static_cast<unsigned int>(hypot(v.x - startPoint.x, v.y - startPoint.y));
      unsigned int curr_dist_end =
        static_cast<unsigned int>(hypot(v.x - endPoint.x, v.y - endPoint.y));      
      
      if (curr_dist_start < start_dist)
        {
          start_dist = curr_dist_start;
          from_vertex = *vertexIt;
        }

      if (curr_dist_end < end_dist)
        {
          end_dist = curr_dist_end;
          to_vertex = *vertexIt;
        }
    }

  Vertex& v1 = graph[from_vertex];
  Vertex& v2 = graph[to_vertex];  
  circle(disconnected, endPoint, 50, Scalar(127), 5);
  circle(disconnected, startPoint, 50, Scalar(127), 5); 
  circle(disconnected, Point2i(v1.x, v1.y), 50, Scalar(255), 5);
  circle(disconnected, Point2i(v2.x, v2.y), 50, Scalar(255), 5);
  

  boost::dijkstra_shortest_paths(graph,
                                 from_vertex,
                                 weight_map(get(&Edge::weight, graph))
                                 .predecessor_map(&p[0]));

  
}

void msk_follow_edge(Mat& disconnected, Mat& seamline, int startx, int starty, Graph& graph)
{
  int x=startx, y=starty;
  bool bFound = true;
  
  while(bFound) {
    bFound = false;
    for(int dy = -1; !bFound && (dy <= 1); dy++)
      for(int dx = -1; !bFound && dx <= 1; dx++)
        if(disconnected.at<uchar>(y+dy, x+dx) == 255)
          {
            disconnected.at<uchar>(y, x) = 128;
            seamline.at<uchar>(y, x) = 255;
            x += dx;
            y += dy;
            bFound = true;
          }
  }
  
  seamline.at<uchar>(y, x) = 255;
  disconnected.at<uchar>(y, x) = 128;
}

struct GeoRef {
	GeoRef(double ox, double oy, double dx, double dy, int cols, int rows) : 
		ox(ox), oy(oy), dx(dx), dy(dy), rows(rows), cols(cols) {
			left = ox;
			top = oy;
			right = ox + dx*cols;
			bottom = oy + dy*rows;
		};
	double ox, oy;
	double dx, dy;
	int rows, cols;
	double top, left, right, bottom;
};


GeoRef read_georef(char* pszFilename)
{
    GDALDataset  *poDataset;
	double adfGeoTransform[6];

    poDataset = (GDALDataset *) GDALOpen( pszFilename, GA_ReadOnly );
    if( poDataset == NULL )
		throw("ERROR: Macagun! No puc obrir el geotiff!\n");

    poDataset->GetGeoTransform( adfGeoTransform );
	int sx = poDataset->GetRasterXSize();
	int sy = poDataset->GetRasterYSize();

	GDALClose( (GDALDatasetH) poDataset );

	return GeoRef(adfGeoTransform[0], adfGeoTransform[3], adfGeoTransform[1], adfGeoTransform[5], sx, sy); 
}


/*
Retorna rectangle interseccio. el punt és el top-left imatge.
*/
Rect2d get_groundcoords_intersection(GeoRef& georef_1, GeoRef& georef_2)
{
	double bottom = max(georef_1.bottom, georef_2.bottom);
	double top = min(georef_1.top, georef_2.top);
	double left = max(georef_1.left, georef_2.left);
	double right = min(georef_1.right, georef_2.right);	
	return Rect2d(left, top, right - left, bottom - top);
}

void read_raster(GeoRef& georef, Rect2d rect, char* fname, Mat& raster)
{
	Point2i pixel_tl(int(float(rect.tl().x - georef.ox) / georef.dx),
					 int(float(rect.tl().y - georef.oy) / georef.dy));

	Point2i pixel_br(int(float(rect.br().x - georef.ox) / georef.dx),
					 int(float(rect.br().y - georef.oy) / georef.dy));

	Mat tmp = imread(fname, 0);	
	tmp.colRange(pixel_tl.x, pixel_br.x).rowRange(pixel_tl.y, pixel_br.y).copyTo(raster);
}


void difima(char* fname_1, char* fname_2, Mat& res)
{
	GeoRef georef_1 = read_georef(fname_1);
	GeoRef georef_2 = read_georef(fname_2);

	Rect2d ground_intersection_rect = get_groundcoords_intersection(georef_1, georef_2);
	printf("%f, %f, %f, %f\n", ground_intersection_rect.tl().x, ground_intersection_rect.tl().y, ground_intersection_rect.br().x, ground_intersection_rect.br().y);

	Mat im1, im2, diff, blurred;
	read_raster(georef_1, ground_intersection_rect, fname_1, im1);
	read_raster(georef_2, ground_intersection_rect, fname_2, im2);
	absdiff(im1, im2, diff);
	blur(diff, blurred, Size(7,7));
	res = (blurred - (im1 == 0) - (im2 == 0)) / 2;
	imwrite("f:\\BuscaCos2\\MYDIFIMA.tif", res);
}


inline unsigned int absdiff(int a, int b)
{
	int d = a-b;
	return ((d >> 30) | 1)*d;
}

void find_nearest_vertex(Point2i point, Mat& landscape, Mat& raster_graph, Mat& breadcrumb)
{
  int x = point.x, y= point.y, max_diff = 0, curr_val, window_val, diff, max_x, max_y, min_val;
  int randx, randy;
  for(int path_len = 512; path_len > 0 && (raster_graph.at<uchar>(y, x) == 0); path_len-- )
    {
      curr_val = landscape.at<uchar>(y, x);
      max_diff = 0;
      min_val = INT_MAX;
      randx = rand();//pseudo_random();
      randy = rand(); //pseudo_random();
      for(int dy = 0; dy < 3; dy++)
        {
          for(int dx = 0; dx < 3; dx++)
            {
              if((dy == 1) && (dx == 1))
                continue;
              
              int iwiny = y + (dy + randy)%3 - 1;
              int iwinx = x + (dx + randx)%3 - 1;
              
              if(breadcrumb.at<uchar>(iwiny, iwinx) == 255) // Ja visitat
                continue;

              if((breadcrumb.at<uchar>(iwiny - 1, iwinx - 1) +
                  breadcrumb.at<uchar>(iwiny - 1, iwinx) +
                  breadcrumb.at<uchar>(iwiny - 1, iwinx + 1) +
                  breadcrumb.at<uchar>(iwiny, iwinx - 1) +
                  breadcrumb.at<uchar>(iwiny, iwinx) +
                  breadcrumb.at<uchar>(iwiny, iwinx + 1) +
                  breadcrumb.at<uchar>(iwiny + 1, iwinx - 1) +
                  breadcrumb.at<uchar>(iwiny + 1, iwinx) +
                  breadcrumb.at<uchar>(iwiny + 1, iwinx + 1)) >= 255*4)
                continue;
              

              
              window_val = landscape.at<uchar>(iwiny, iwinx);
              // Anem cap avall. L'actual ha de ser mes alt...
              // diff =  absdiff(curr_val, window_val); //curr_val - window_val;
              printf("(%d, %d) = %d, %d -> %d - %d\n", (dx + randx)%3 - 1, (dy + randy)%3 - 1, iwinx, iwiny, curr_val, window_val);
              
              if(window_val < min_val)
                {
                  //                  diff = max_diff;
                  min_val = window_val;
                  max_x = iwinx;
                  max_y = iwiny;
                }
            }
        }

      breadcrumb.at<uchar>(y, x) = 255;
      x = max_x;
      y = max_y;
      printf("\n");
    }
}

int main(int argc, char **argv)
{
    GDALAllRegister();
    /*
    char* fname_1 = "83690004.tif";
    char* fname_2 = "83690005.tif";
    83610025
    */
    char* fname_1 = "83610025.tif";
    char* fname_2 = "83620024.tif";
    
    Graph graph;
    Mat raster_graph, vertexs, vertex_labels, costImg;
  
  /*
  Mat imgMaxLevel;
  costImg = imread("COST.tif", 0);
  Mat element = getStructuringElement(MORPH_RECT, Size(3, 3), Point(2, 2));  
  dilate((costImg >= 22) - (costImg > 32), imgMaxLevel, element, Point(-1, -1), 2);
  imwrite("threshold.tif", imgMaxLevel);  
  return 0;
  */
  
  if(true)                     // Per debugar més ràpid...
    {
      //read_cost_img("PP.tif", costImg);
      
	  difima(fname_1, fname_2, costImg);
	  imwrite("COST.tif", costImg);
      get_raster_graph(costImg, raster_graph,
                       Point2i(costImg.cols / 2, costImg.rows / 4),
                       Point2i(3*costImg.cols / 4, 3*costImg.rows / 4));

      // imwrite("raster_graph.tif", raster_graph);
      // return(0);
    }
  else
    {
	  raster_graph = imread("RASTER_GRAPH_ORIG.tif", 0);
      costImg = imread("COST.tif", 0);
    }
  
  Point2i startPoint(costImg.cols / 2, costImg.rows / 4);
  Point2i endPoint(3*costImg.cols / 4, 3*costImg.rows / 4);

  mark_vertexs(raster_graph, vertexs);
//  imwrite("VERTEXS.tif", vertexs);
  vertexs.copyTo(vertex_labels);
  Mat element = getStructuringElement(MORPH_ELLIPSE, Size(5, 5), Point(2, 2));
  dilate(vertex_labels, vertex_labels, element, Point(-1,-1), 1);
  imwrite("VERTEX_LABELS.tif", vertex_labels);

//  label_dilated_vertexs(vertexs, vertex_labels, graph);


  Mat disconnected(raster_graph - (vertexs > 0));
  if(disconnected.type() == CV_8U)
    printf("Tipus guai\n");
  Mat disconnected_backup;
  disconnected.copyTo(disconnected_backup);
  
  build_edges(disconnected, costImg, vertex_labels, graph);
  
  vector<int> predecessor(num_vertices(graph)); //the predecessor array
  VertexID from_vertex, to_vertex;
  calculate_dijkstra(predecessor,
                     from_vertex, to_vertex,
                     startPoint, endPoint,
                     disconnected, graph);

  //  imwrite("DISCONNECTED.tif", disconnected);

  // return(0);

  // Aixo dibuixa el graf... va b per debugar
  // Recorrem el cami trobat
  imwrite("RASTER_GRAPH.tif", raster_graph);
  Mat seamline(disconnected.rows, disconnected.cols, CV_8U);
  seamline = Scalar::all(0);
  EdgeID edgeId;
  bool ok;
  for(VertexID vId = to_vertex; vId != from_vertex; vId = predecessor[vId])
    {
      Vertex& v = graph[vId];
	  boost::tie(edgeId, ok) = edge(vId, predecessor[vId], graph);
	  if(ok)
	  {
		  Edge& e = graph[edgeId];
		  e.drawLine(disconnected, 2);

		  if(hypot(e.x1 - v.x, e.y1 - v.y) < hypot(e.x2 - v.x, e.y2 - v.y))
			msk_follow_edge(disconnected_backup, seamline, e.x1, e.y1, graph);
		  else
			msk_follow_edge(disconnected_backup, seamline, e.x2, e.y2, graph);
	  }

	  if(vId == predecessor[vId])
	  {
		  throw CosException("Error: els dos extrems no han quedat connectats pel graf\n");
	  }

    }

  printf("Nearest vertex...\n");
  imwrite("RASTER2.tif", raster_graph);
  find_nearest_vertex(startPoint, costImg, raster_graph, seamline);


  vector<Mat> channels(3);
  channels[2] = (disconnected + seamline);
  channels[1] = (disconnected - seamline);
  channels[0] = (disconnected - seamline);  
  Mat toDump;
  merge(channels, toDump);
  imwrite("disconnected.tif", toDump);
  
  return 0;
}
